import { Injectable } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';

@Injectable({ providedIn: 'root' })
export class HeroesService {
  private heroes: Heroe[] = [
    {
      nombre: 'Aquaman',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/aquaman.png',
      aparicion: '1941-123-123',
      casa: 'DC',
    },
    {
      nombre: 'Batman',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/batman.png',
      aparicion: '1939-123-123',
      casa: 'DC',
    },
    {
      nombre: 'Daredevil',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/daredevil.png',
      aparicion: '1964-123-123',
      casa: 'DC',
    },
    {
      nombre: 'Hulk',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/hulk.png',
      aparicion: '1962-123-123',
      casa: 'Marvel',
    },
    {
      nombre: 'Linterna Verde',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/linterna-verde.png',
      aparicion: '1940-123-123',
      casa: 'DC',
    },
    {
      nombre: 'Spider-Man',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/spiderman.png',
      aparicion: '1962-123-123',
      casa: 'Marvel',
    },
    {
      nombre: 'Wolverine',
      bio:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      img: 'assets/img/wolverine.png',
      aparicion: '1974-123-123',
      casa: 'Marvel',
    },
  ];

  constructor() {
    console.log('Servicio listo');
  }

  getHeroes(): Heroe[] {
    return this.heroes;
  }

  getHeroe(idx: string) {
    return this.heroes[idx];
  }
}

export interface Heroe {
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  casa: string;
}
